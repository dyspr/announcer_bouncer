var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var announcer
var steps = 0

function preload() {
  announcer = loadImage('img/fortepan_187980.jpg')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    image(announcer, - boardSize * 0.4 * (1 - i / steps), - boardSize * 0.4 * (1 - i / steps), boardSize * 0.8 * (1 - i / steps), boardSize * 0.8 * (1 - i / steps))
    pop()
  }

  steps = 2 + abs(sin(cos(frameCount * 0.01 + Math.PI * 0.5) * Math.PI) * 30)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
